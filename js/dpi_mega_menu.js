jQuery(document).ready(function (jQuery) {
    var $width = 0;

    jQuery(window).scroll(function () {
        var mobileEnable = jQuery('.main_menu_list').find('a.active').length != 0 ? true : false;   //  lets us know if the mobile menu is active at all

        if ((jQuery(this).scrollTop() > 126 && $width >= 1300) || (!mobileEnable && $width < 1300 && jQuery(this).scrollTop() > 126)) {
            //  if it is a desktop version and scrolled down, or it if the mobile menu is not active and it is scrolled down -> sitck it
            jQuery('#main_menu_wrapper').addClass("sticky");
        } else {
            //  unstick it
            jQuery('#main_menu_wrapper').removeClass("sticky");
        }
    });

    //  if the windows is resized
    jQuery(window).resize(function () {
        $width = jQuery('html').width(); //  gets the current width

        //  will remove the inline styling of the toggleSlide function from going to and from mobile
        if ($width >= 1300) {
            jQuery(".mega_menu_selector").removeClass("active");
            jQuery('.main_menu_list').attr('style', "");
            jQuery('.row_mega_menu').attr('style', "");
        }
    });

    jQuery(document).click(function (event) {
        $width = jQuery('html').width(); //  gets the current width

        var $baseTarget = jQuery(event.target), itemID; //  gets the DOM element that was clicked

        $target = $baseTarget.is('a.mega_menu_selector') ? $baseTarget : $baseTarget.closest('a.mega_menu_selector');   //  if the element that click was not the a tag get the closest one
        $activeTarget = $baseTarget.is('div.active') ? $baseTarget : $baseTarget.closest('div.active');   //  if the element that click was the active menu box

        // if the element that was clicked is indeed a menu title open or close the tabs
        if ($target.is('a.mega_menu_selector')) {
            var key = jQuery($target).data("toggle-id");   //  get the custom post id
            var siblingKey = '.row_' + key;    //  sets the class element to get
            var sibling = jQuery($target).siblings(siblingKey);

            //  if the display is mobile or not, interact differently
            if ($width >= 1300) {
                var siblingClass = jQuery($target).siblings(siblingKey).attr("class");  //  get the next DOM object, which will be the div container

                //  if the element is active, deactivate it or activate it
                if (siblingClass.indexOf("active") !== -1) {
                    jQuery($target).siblings(siblingKey).removeClass("active");
                    jQuery($target).removeClass("active_tab");
                } else {
                    jQuery(".row_mega_menu").removeClass("active"); //  removes the active class form all mega menu rows
                    jQuery(".mega_menu_selector").removeClass("active_tab");
                    jQuery($target).siblings(siblingKey).addClass("active");   //  adds the active class
                    jQuery($target).addClass("active_tab");
                }
            } else {
                //  else it is mobile and activate the jQuery slide function
                jQuery(sibling).slideToggle(500, 'swing');

                $currentClass = jQuery($target).attr("class");  //  gets the current class

                //  if the element is selected either change the state to active or not
                //  will keep all active elements active till the user closes them
                if ($currentClass.indexOf("active") !== -1) {
                    jQuery($target).removeClass("active");
                } else {
                    jQuery($target).addClass("active");
                }
            }
        } else if ($activeTarget.is('div.active')) {
            //  do nothing if anything in the active menu box was clicked
        } else {
            //  if an element was clicked outside the active menu box
            jQuery(".row_mega_menu").removeClass("active"); //  removes the active class form all mega menu rows
            jQuery(".mega_menu_selector").removeClass("active_tab");
        }

    });
    //  for the mobile menu, the built in jQuery for sliding div's
    jQuery('.toggle').click(function () {
        jQuery('.main_menu_list').slideToggle(500, 'swing');
    });
});