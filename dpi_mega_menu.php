<?php
/**
 * Plugin Name: DPI Mega Menu
 * Plugin URI: http://www.diocesan.com/
 * Description: This plugin creates a mega menu
 * Version: 1.0.3
 * Author: Diocesan Publications
 * Author URI: http://www.diocesan.com/
 * License: GPL2
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       dpi_mpa
 * Bitbucket Plugin URI: https://bitbucket.org/Admiral_Scott/dpi_mpa
 * Bitbucket Branch:     master
*/

function enqueue_script()
{
    wp_enqueue_script( 'dpi_mega_menu_script', plugins_url( '/js/dpi_mega_menu.js', __FILE__ ), array('jquery') );
}
add_action('wp_enqueue_scripts','enqueue_script');

function mega_menu_enqueue_stylesheets() {
    wp_enqueue_style( 'dpi_mega_menu_style', plugins_url( '/css/dpi_mega_menu.css', __FILE__ ), false, '1.0', 'all');	//	the front end css
}
add_action( 'wp_enqueue_scripts', 'mega_menu_enqueue_stylesheets', 1 ); //  loads this sheet first to overwrite in theme css

/* ============================================ */
/*      Custom Post Type for Mega Menu          */
/* ============================================ */

//function that displays post editor on about custom post
function dpi_mega_menu_function() {
    $labels = array(
    'name'               => _x( "DPI Mega Menu", 'post type general name' ),
    'singular_name'      => _x( "DPI Mega Menu", 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( "Add New DPI Mega Menu Heading" ),
    'edit_item'          => __( "Edit DPI Mega Menu" ),
    'new_item'           => __( "New DPI Mega Menu" ),
    'all_items'          => __( "All DPI Mega Menu" ),
    'view_item'          => __( "View DPI Mega Menu" ),
    'search_items'       => __( "Search DPI Mega Menu" ),
    'not_found'          => __( "No DPI Mega Menu Found" ),
    'not_found_in_trash' => __( "No DPI Mega Menu found in the Trash" ),
    'parent_item_colon'  => '',
    'menu_name'          => "DPI Mega Menu"
    );
    
    //pulls wordpress default editor options
    $args = array(
    'labels'        => $labels,
    'description'   => "holds database of created DPI Mega Menu",
    'public'        => true,
    'show_ui'       => true,
    'show_in_menu'  => true,
    'menu_position' => 5,
    'supports'      => array( 'title' ),
    'has_archive'   => true,
    );
    register_post_type( 'dpi_mega_menu', $args );
}
//initialize on page content
add_action( 'init', 'dpi_mega_menu_function' );

/* the Meta Box stuff */
function dpi_mega_menu_meta_box_markup($object)
{
    global $post;
    
    $sub_title = get_post_meta($post->ID, '_mega_menu_sub_title', true);
    $include = get_post_meta($post->ID, '_mega_menu_include', true);
    
    $left_side = get_post_meta($post->ID, '_mega_menu_left', true);
    $left_side_shortcode = get_post_meta($post->ID, '_mega_menu_left_shortcode', true);
    
    $menu_first = get_post_meta($post->ID, '_mega_menu_first', true);
    $menu_first_text = get_post_meta($post->ID, '_mega_menu_first_text', true);
    $menu_first_shortcode = get_post_meta($post->ID, '_mega_menu_first_shortcode', true);
    
    $menu_second = get_post_meta($post->ID, '_mega_menu_second', true);
    $menu_second_text = get_post_meta($post->ID, '_mega_menu_second_text', true);
    $menu_second_shortcode = get_post_meta($post->ID, '_mega_menu_second_shortcode', true);
    
    $menus = get_registered_nav_menus();
    
    $wpEditorSettings = array(
    'quicktags' => array('buttons' => 'em,strong,link',),
    'quicktags' => true,
    'tinymce' => array(
    'width' => 200
    ),
    'tinymce' => true,
    'drag_drop_upload' => true,
    );
    
    ?>
  <input type="hidden" name="mega_menu_meta_box_nonce" id="mega_menu_meta_box_nonce" value="<?php echo wp_create_nonce('mega_menu');?>" />
  <label for="_mega_menu_description">
    <h4>Sub Title</h4></label>
  <input type="text" name="_mega_menu_sub_title" id="_mega_menu_sub_title" class="widefat" value="<?php echo $sub_title;?>" />
  <label for="">
    <h4>Include in Mega Menu</h4></label>
  <input type="checkbox" name="_mega_menu_include" id="_mega_menu_include" class="widefat" <?php echo $include==="on" ? "checked" : ""; ?> />
  <hr>
  <label for="_mega_menu_left">
    <h4>Left Side Text</h4></label>
  <?php wp_editor( $left_side, '_mega_menu_left', $wpEditorSettings ); ?>
    <label for="_mega_menu_left_shortcode">
      <h4>Left Side Shortcode</h4></label>
    <input type="text" name="_mega_menu_left_shortcode" id="_mega_menu_left_shortcode" class="widefat" value="<?php echo $left_side_shortcode;?>" />
    <hr>
    <label for='_mega_menu_first_text'>
      <h4>First Menu Text</h4></label>
    <?php wp_editor( $menu_first_text, '_mega_menu_first_text', $wpEditorSettings ); ?>
      <label for='_mega_menu_first'>
        <h4>First Menu</h4></label>
      <select name="_mega_menu_first" class="widefat">
        <option value=""></option>
        <?php
    if (count($menus) > 0) {
        foreach ( $menus as $location => $description ) {
            ?>
          <option value="<?php echo $location;?>" <?php echo $menu_first===$location ? "selected" : ""; ?>>
            <?php echo $description;?>
          </option>
          <?php
        } //  end foreach ( $menus as $location => $description )
    } //  end if (count($menus) > 0)
    ?>
      </select>
      <label for="_mega_menu_first_shortcode">
        <h4>First Menu Shortcode</h4></label>
      <input type="text" name="_mega_menu_first_shortcode" id="_mega_menu_first_shortcode" class="widefat" value="<?php echo $menu_first_shortcode;?>" />
      <hr>
      <label for='_mega_menu_second_text'>
        <h4>Second Menu Text</h4></label>
      <?php wp_editor( $menu_second_text, '_mega_menu_second_text', $wpEditorSettings ); ?>
        <label for='_mega_menu_second'>
          <h4>Second Menu</h4></label>
        <select name="_mega_menu_second" class="widefat">
          <option value=""></option>
          <?php
    if (count($menus) > 0) {
        foreach ( $menus as $location => $description ) {
            ?>
            <option value="<?php echo $location;?>" <?php echo $menu_second===$location ? "selected" : ""; ?>>
              <?php echo $description;?>
            </option>
            <?php
        } //  end foreach ( $menus as $location => $description )
    } //  end if (count($menus) > 0)
    ?>
        </select>
        <label for="_mega_menu_second_shortcode">
          <h4>Second Menu Shortcode</h4></label>
        <input type="text" name="_mega_menu_second_shortcode" id="_mega_menu_second_shortcode" class="widefat" value="<?php echo $menu_second_shortcode;?>" />
        <?php
}

function add_dpi_mega_menu_meta_box()
{
    add_meta_box("dpi_mega_menu_meta_box", "Create the Menu", "dpi_mega_menu_meta_box_markup", "dpi_mega_menu", "normal", "high", null);
}
add_action("add_meta_boxes", "add_dpi_mega_menu_meta_box");

function save_dpi_mega_menu_meta_box($post_id, $post)
{
    /*  The First Menu Selection    */
    
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if ( !wp_verify_nonce( $_POST['mega_menu_meta_box_nonce'], 'mega_menu' )) {
        return $post->ID;
    } //  end if ( !wp_verify_nonce( $_POST['mega_menu_meta_box_nonce'], 'mega_menu' ))
    
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID )) {
        return $post->ID;
    } //  end if ( !current_user_can( 'edit_post', $post->ID ))
    
    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though.
    $menu_meta['_mega_menu_sub_title'] = $_POST['_mega_menu_sub_title'];
    $menu_meta['_mega_menu_include'] = $_POST['_mega_menu_include'];
    $menu_meta['_mega_menu_left'] = $_POST['_mega_menu_left'];
    $menu_meta['_mega_menu_left_shortcode'] = htmlentities($_POST['_mega_menu_left_shortcode']);
    $menu_meta['_mega_menu_first'] = $_POST['_mega_menu_first'];
    $menu_meta['_mega_menu_first_text'] = $_POST['_mega_menu_first_text'];
    $menu_meta['_mega_menu_first_shortcode'] = htmlentities($_POST['_mega_menu_first_shortcode']);
    $menu_meta['_mega_menu_second'] = $_POST['_mega_menu_second'];
    $menu_meta['_mega_menu_second_text'] = $_POST['_mega_menu_second_text'];
    $menu_meta['_mega_menu_second_shortcode'] = htmlentities($_POST['_mega_menu_second_shortcode']);
    
    // Add values of $link_meta as custom fields
    foreach ($menu_meta as $key => $value) { // Cycle through the $link_meta array!
        save_custom_post_meta($post, $value, $key);
    } //  end foreach ($menu_meta as $key => $value)
}
add_action("save_post", "save_dpi_mega_menu_meta_box", 1, 2);

function save_custom_post_meta($post, $value, $key)
{
    if( $post->post_type == 'revision' ) {
        return; // Don't store custom data twice
    } //  end if( $post->post_type == 'revision' )
    $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
    if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
        update_post_meta($post->ID, $key, $value);
    } else { // If the custom field doesn't have a value
        add_post_meta($post->ID, $key, $value);
    } //  end if(get_post_meta($post->ID, $key, FALSE))
    if (!$value) {
        delete_post_meta($post->ID, $key); // Delete if blank
    } //  end if (!$value)
}

//  replaces the standard push pin icon with the 3 bar meny icon
function replace_admin_mega_menu_icons_css() {
    ?>
    <style>
      #menu-posts-dpi_mega_menu div.wp-menu-image::before {
        content: "\f333";
      }
    </style>
    <?php
}

add_action( 'admin_head', 'replace_admin_mega_menu_icons_css' );

//Including file that manages all template
require_once plugin_dir_path( __FILE__ ) . 'dpi_mega_menu_shortcode.php';

//  including file that manages the admin side
require_once plugin_dir_path( __FILE__ ) . 'includes/class_dpi_mega_menu.php';

/**
* Begins execution of the plugin.
*
* Since everything within the plugin is registered via hooks,
* then kicking off the plugin from this point in the file does
* not affect the page life cycle.
*
* @since    1.0
*/
function run_dpi_mega_menu() {
    $plugin = new DPI_Mega_Menu();
    $plugin->run();
}
run_dpi_mega_menu();