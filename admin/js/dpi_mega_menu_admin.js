(function ($) {
    //  allows the ordering of the menu items
    'use strict';
    $(function () {
        // Fix broken table width on sort
        $('.sslp-order td').each(function () {
            $(this).css('width', $(this).width() + 'px');
        });

        // Sortable Table
        if ($('.sslp-order#sortable-table').length > 0) {
            $('.sslp-order#sortable-table tbody').sortable({
                axis: 'y',
                handle: '.dpi_mm_column_order img',
                placeholder: 'ui-state-highlight',
                forcePlaceholderSize: true,
                update: function (event, ui) {
                    var theOrder = $(this).sortable('toArray');

                    var data = {
                        action: 'update_menu_order',
                        postType: 'dpi_mega_menu',
                        order: theOrder
                    };

                    jQuery.post(ajaxurl, data, function (response) {
                        //  alert('Got this from the server: ' + response);
                    });
                }
            }).disableSelection();
        }
        // Collapsible divs on templates page
        $(".content").hide();
        $(".heading").click(function () {
            console.log("Toggle");
            $(this).next(".content").slideToggle(500);
        });
    });
})(jQuery);
