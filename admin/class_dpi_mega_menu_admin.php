<?php

/**
* The admin-specific functionality of the plugin.
*
* @link       http://www.diocesan.com
* @since      1.0
*
* @package    DPI_Mega_Menu
* @subpackage DPI_Mega_Menu/includes
*/

/**
* The admin-specific functionality of the plugin.
*
* Defines the plugin name, version, and two examples hooks for how to
* enqueue the admin-specific stylesheet and JavaScript.
*
* @since      1.0
* @package    DPI_Mega_Menu
* @subpackage DPI_Mega_Menu/includes
* @author     Scott Schwab <scschwab@gmail.com>
*/

class DPI_Mega_Menu_Admin {
    /**
    * The ID of this plugin.
    *
    * @since    1.0
    * @access   private
    * @var      string $plugin_name The ID of this plugin.
    */
    private $plugin_name;
    
    /**
    * The version of this plugin.
    *
    * @since    1.0
    * @access   private
    * @var      string $version The current version of this plugin.
    */
    private $version;
    
    /**
    * Initialize the class and set its properties.
    *
    * @since    1.0
    *
    * @param      string $plugin_name The name of this plugin.
    * @param      string $version The version of this plugin.
    */
    public function __construct( $plugin_name, $version ) {
        $this->plugin_name = $plugin_name;
        $this->version     = $version;
    }
    
    /**
    * Register the stylesheets for the admin area.
    *
    * @since    1.0
    */
    public function enqueue_styles() {
        /**
        * This function is provided for demonstration purposes only.
        *
        * An instance of this class should be passed to the run() function
        * defined in DPI_Mega_Menu_Loader as all of the hooks are defined
        * in that particular class.
        *
        * The DPI_Mega_Menu_Loader will then create the relationship
        * between the defined hooks and the functions defined in this
        * class.
        */
        
        wp_enqueue_style( $this->plugin_name,
        plugin_dir_url( __FILE__ ) . 'css/dpi_mega_menu_admin.css',
        array(),
        $this->version,
        'all' );
    }
    
    /**
    * Register the JavaScript for the admin area.
    *
    * @since    1.0
    */
    public function enqueue_scripts() {
        
        /**
        * This function is provided for demonstration purposes only.
        *
        * An instance of this class should be passed to the run() function
        * defined in DPI_Mega_Menu_Loader as all of the hooks are defined
        * in that particular class.
        *
        * The DPI_Mega_Menu_Loader will then create the relationship
        * between the defined hooks and the functions defined in this
        * class.
        */
        
        if ( isset($_GET['post_type']) && $_GET['post_type'] == 'dpi_mega_menu' ) {
            wp_enqueue_script( $this->plugin_name,
            plugin_dir_url( __FILE__ ) . 'js/dpi_mega_menu_admin.js',
            array( 'jquery', 'jquery-ui-core', 'jquery-ui-sortable' ),
            $this->version,
            true );
        }
    }
    
    /**
    * Register admin menu items.
    *
    * @since   1.0
    */
    public function register_menu() {
        // Order page
        add_submenu_page(
        'edit.php?post_type=dpi_mega_menu',
        __( 'Mega Menu Order', $this->plugin_name ),
        __( 'Order', $this->plugin_name ),
        'edit_pages',
        'menu_order',
        array( $this, 'display_order_page' )
        );
    }
    
    /**
    * Display Order page content.
    *
    * @since   1.0
    */
    public function display_order_page() {
        include_once( 'partials/dpi_mega_menu_order_display.php' );
    }
    
    /**
    * Update Menu order.
    *
    * @since 1.0
    *
    * @return mixed
    */
    public function update_menu_order() {
        global $wpdb;
        
        $post_type  = $_POST['postType'];
        $order      = $_POST['order'];
        
        foreach( $order as $menu_order => $post_id )
        {
            $post_id         = intval( str_ireplace( 'post-', '', $post_id ) );
            $menu_order     = intval($menu_order);
            $my_post = array(
            'ID' => $post_id,
            'menu_order' => $menu_order,
            'post_content' => 'This is the updated content.',
            );
            wp_update_post( $my_post );
        }
        wp_die(); // this is required to terminate immediately and return a proper response
    }
    
    //  replaces the standard push pin icon with the 3 bar meny icon
    public function replace_admin_mega_menu_icons_css() {
        ?>
  <style>
    #menu-posts-dpi_mega_menu div.wp-menu-image::before {
      content: "\f333";
    }
  </style>
  <?php
    }
}