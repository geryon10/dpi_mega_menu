<?php

/**
* Provide a admin area view for the plugin
*
* This file is used to markup the admin-facing aspects of the plugin.
*
* @link       http://www.diocesan.com
* @since      1.0.0
*
* @package    DPI_Mega_Menu
* @subpackage DPI_Mega_Menu/admin/partials
*/
?>
  <div class="wrap">
    <?php wp_reset_postdata(); // Don't forget to reset again! ?>
      <h2><?php _e( 'Order Mega Menu', 'dpi_mega_menu' ); ?></h2>
      <p>
        <?php _e( 'Simply drag the menu item up or down and they will be saved in that order.', 'dpi_mega_menu' ); ?>
      </p>
      <?php
$menu = new WP_Query( array( 'post_type' => 'dpi_mega_menu', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'menu_order' ) );
if( $menu->have_posts() ) {
    ?>
        <table class="wp-list-table widefat fixed posts sslp-order" id="sortable-table">
          <tbody data-post-type="product">
            <?php
    while ( $menu->have_posts() ) {
        $menu->the_post();
        global $post;
        $custom = get_post_custom();
        ?>
              <tr id="post-<?php the_ID(); ?>">
                <td class="dpi_mm_column_order"><img src="<?php echo STAFFLIST_URI . 'admin/img/move-icon.png'; ?>" title="" alt="Move Icon" width="24" height="24" class="" /></td>
                <td class="dpi_mm_column_name"><strong><?php the_title(); ?></strong></td>
              </tr>
              <?php
    }   //  end while ( $menu->have_posts() )
    ?>
          </tbody>
        </table>
        <?php
} else {
    ?>
          <p>
            <?php _e( 'No menu items found, why not <a href="post-new.php?post_type=dpi_mega_menu">create one?', 'dpi_mega_menu' ); ?></a>
          </p>
          <?php
}   //  end if( $menu->have_posts() )
?>
  </div>
  <!-- .wrap -->