<?php
/*
Testing to see if this update thing works
Second Test
*/
function dpi_mega_menu() {
    $h = new WP_Query( array(
    'no_found_rows' => true,
    'post_status' => 'publish',
    'post_type' => 'dpi_mega_menu',
    'ignore_sticky_posts' => true,
    'orderby' => 'menu_order',
    'order' => 'ASC',
    ) );
    ?>
  <div class="main_menu_wrapper" id="main_menu_wrapper">
    <h2 class="toggle">Menu<span class="menu_icon"></span></h2>
    <ul class="main_menu_list">
      <?php
    if ($h->have_posts()) {
        while ( $h->have_posts() ) {
            $h->the_post();
            $sub_title = html_entity_decode(get_post_meta(get_the_ID(), '_mega_menu_sub_title', true));
            $include = get_post_meta(get_the_ID(), '_mega_menu_include', true); //  if the menu item is to be displayed, if it is not skip the next part
            
            if ($include === "on") {
                $menu_left = get_post_meta(get_the_ID(), '_mega_menu_left', true);
                $left_shortcode = html_entity_decode(get_post_meta(get_the_ID(), '_mega_menu_left_shortcode', true));
                
                $menu_first = get_post_meta(get_the_ID(), '_mega_menu_first', true);
                $menu_first_text = html_entity_decode(get_post_meta(get_the_ID(), '_mega_menu_first_text', true));
                $menu_first_shortcode = html_entity_decode(get_post_meta(get_the_ID(), '_mega_menu_first_shortcode', true));
                
                $menu_first_content = $menu_first != "" || $menu_first_text != "" || $menu_first_shortcode != "" ? true : false;  //  sees if anything should be on the first menu column
                
                $menu_second = get_post_meta(get_the_ID(), '_mega_menu_second', true);
                $menu_second_text = html_entity_decode(get_post_meta(get_the_ID(), '_mega_menu_second_text', true));
                $menu_second_shortcode = html_entity_decode(get_post_meta(get_the_ID(), '_mega_menu_second_shortcode', true));
                
                $menu_second_content = $menu_second != "" || $menu_second_text != "" || $menu_second_shortcode != "" ? true : false; //  sees if anything should be on the second column
                
                if ($menu_first_content && $menu_second_content) {
                    //  if there is content in the first and second column set the correct classes
                    $menu_first_class = "menu_parts";
                    $menu_second_class = "menu_parts";
                    $menu_description_class = "menu_description";
                } elseif ($menu_first_content && !$menu_second_content) {
                    //  if there is only content on the first column, set the correct classes
                    $menu_first_class = "menu_parts_solo";
                    $menu_second_class = "menu_parts_none";
                    $menu_description_class = "menu_description";
                } elseif (!$menu_first_content && $menu_second_content) {
                    //  if there is only content on the second column, set the correct classes
                    $menu_first_class = "menu_parts_none";
                    $menu_second_class = "menu_parts_solo";
                    $menu_description_class = "menu_description";
                } elseif (!$menu_first_content && !$menu_second_content) {
                    //  if there is no content in both the frist and second column, set the correct classes
                    $menu_first_class = "menu_parts_none";
                    $menu_second_class = "menu_parts_none";
                    $menu_description_class = "menu_description_full";
                }
                ?>
        <li>
          <a class="mega_menu_selector" data-toggle-id="<?php echo get_the_ID();?>"><h2 class="menu_li_title"><?php the_title();?></h2><h6 class="menu_li_sub_title"><?php echo $sub_title;?></h6></a>
          <div class="row_mega_menu row_<?php echo get_the_ID();?>">
            <div class="<?php echo $menu_description_class;?>">
              <div>
                <?php echo apply_filters( 'the_content', $menu_left );?>
              </div>
              <?php echo do_shortcode($left_shortcode);?>
            </div>
            <div class="menu_navigation_row">
              <div class="<?php echo $menu_first_class;?>">
                <div>
                  <?php echo apply_filters( 'the_content', $menu_first_text);?>
                </div>
                <div>
                  <?php
                if ( has_nav_menu( $menu_first ) ) {
                    wp_nav_menu( array(
                    'theme_location'  => $menu_first,
                    'menu_class'      => 'first-menu',
                    'depth'           => 0,
                    ) );
                } //  end if ( has_nav_menu( $menu_first ) )
                ?>
                    <div>
                      <?php echo do_shortcode($menu_first_shortcode);?>
                    </div>
                </div>
              </div>
              <div class="<?php echo $menu_second_class;?>">
                <div>
                  <?php echo apply_filters( 'the_content', $menu_second_text);?>
                </div>
                <div>
                  <?php
                if ( has_nav_menu( $menu_second ) ) {
                    wp_nav_menu( array(
                    'theme_location' => $menu_second,
                    'menu_class'     => 'second-menu',
                    ) );
                } // end  if ( has_nav_menu( $menu_second ) )
                ?>
                </div>
                <div>
                  <?php echo do_shortcode($menu_second_shortcode);?>
                </div>
              </div>
            </div>
          </div>
        </li>
        <?php
            }  //  if the menu item is to be included
        }  //  the post while loop
    }  //  if there is posts
    ?>
    </ul>
  </div>
  <?php
}
add_shortcode( 'dpi_mega_menu', 'dpi_mega_menu' );